import { Injectable } from '@angular/core';
import { Course } from './course';
import { COURSES } from './mock-courses';

@Injectable()
export class CourseService {

  getAll(): Promise<Course[]>  {
    return new Promise(resolve => 
      resolve(COURSES)
    );
  }

  getById(id): Promise<Course> {
    return new Promise(resolve => 
      resolve(COURSES.find(course => course.id === id))
    );
  }

  add(data) {
    return new Promise(resolve => {
      COURSES.push(data);
      resolve(data);
    });
  }

  delete(id) {
    return new Promise(resolve => {
      let index = COURSES.findIndex(course => course.id === id);
      COURSES.splice(index, 1);
      resolve(true);
    });
  } 

  update(data) {
    return new Promise(resolve => {
     
      resolve(data);
    });
  }

  
  constructor() { }

}
