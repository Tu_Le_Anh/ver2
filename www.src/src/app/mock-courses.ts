import { Course } from './course';

export const COURSES: Course[] = [
    {
        id: 1,
        name: 'Course #1',
    },
    {
        id: 2,
        name: 'Course #2',
    },
    {
        id: 3,
        name: 'Course #3',
    },
    {
        id: 4,
        name: 'Course #4',
    },
  
    
];
    