export class Word {
    public word: string;
    public mean: string;
    constructor(word:string,mean:string) {
        this.word = word;
        this.mean = mean;
    }
}