import { Component, OnInit } from '@angular/core';
import { Course } from '../course';
import { CourseService } from '../course.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  
  course : Course[];
  id: number = 10;
  constructor(private courseService: CourseService) { }

  ngOnInit() {
    this.getCourses()
  }

  getCourses(): void {
    this.courseService.getAll()
    .then(courses => this.course = courses);
  }
  
  delete(item): void {
    // const id = +this.route.snapshot.paramMap.get('id');
    this.courseService.delete(item.id).then(() => {
      return this.getCourses();
    });
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    var newCourse = new Course;
    newCourse.id = this.id++;
    newCourse.name = name;
    this.courseService.add(newCourse).then();
  }
}
