import { Component, OnInit, Inject, Input } from '@angular/core';
import {FormControl} from '@angular/forms';
import { Word } from '../word';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css']
})
export class CategoryDetailComponent implements OnInit {

  words: Word[] = [];
  showDialog = false;
  constructor() { }

  
  ngOnInit() {
  }

  AddWord(word: string, mean: string) {
    console.log(word,mean)
    this.showDialog=false;
    var newword = new Word(word, mean);

    console.log(newword);
    this.words.push(newword);
  }
}


