import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CoursesComponent } from './courses/courses.component';
import { CoursedetailComponent} from './coursedetail/coursedetail.component';
import { CategoryDetailComponent } from './category-detail/category-detail.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'register', component: RegisterComponent },
  //{ path: 'courses', component: CoursesComponent },
  { path: 'detail/:id', component: CoursedetailComponent },
  { path: 'category_detail/:name', component: CategoryDetailComponent },

  // otherwise redirect to home
  //{ path: '**', redirectTo: '' }
];

//@NgModule({

  //imports: [RouterModule.forRoot(routes)],
  //exports: [RouterModule]

//})
//export class AppRoutingModule { }

export const routing = RouterModule.forRoot(routes);