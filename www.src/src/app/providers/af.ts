import {Injectable} from "@angular/core";
import { FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2/es2015';
import {FirebaseObjectFactoryOpts} from 'angularfire2/es2015';
import { AngularFireAuth } from 'angularfire2/auth';
import {FormBuilder, FormGroup, Validators, AbstractControl,FormControl} from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AuthProvider } from '../providers/auth/auth';
import { Promise } from "q";

@Injectable()
export class AF {
  public messages: FirebaseListObservable<any>;
  public users: FirebaseListObservable<any>;
  public displayName: string;
  public email: string;
  public user: FirebaseObjectObservable<any>;

  constructor(public af: AngularFireAuth,public db: AngularFireDatabase) {
    this.af.authState.subscribe(
      (auth) => {
        if (auth != null) {
          this.user = db.object('users/' + auth.uid);
        }
      });

    this.messages = db.list('messages');
    this.users = db.list('users');
  }

 

  
  addUserInfo(){
    //We saved their auth info now save the rest to the db.
    this.users.push({
      email: this.email,
      displayName: this.displayName
    });
  }

  /**
   * Saves a message to the Firebase Realtime Database
   * @param text
   */
  sendMessage(text) {
    var message = {
      message: text,
      displayName: this.displayName,
      email: this.email,
      timestamp: Date.now()
    };
    this.messages.push(message);
  }

  /**
   *
   * @param model
   * @returns {firebase.Promise<void>}
   */
  registerUser(email, password) {
    console.log(email)
    return this.af.auth.createUserWithEmailAndPassword(
      email,
      password
    );
  }

  /**
   *
   * @param uid
   * @param model
   * @returns {firebase.Promise<void>}
   */
  saveUserInfoFromForm(uid, fullname, email) {
    return this.db.object('registeredUsers/' + uid).set({
      username: fullname,
      email: email,
      avatar: null,
    });
  }



}
