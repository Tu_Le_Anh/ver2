import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';

import * as firebase from 'firebase/app';
import { User } from "../models/user";
import { AngularFireAuth } from 'angularfire2/auth';
import {FormBuilder, FormGroup, Validators, AbstractControl,FormControl} from '@angular/forms';

import { AuthProvider } from '../providers/auth/auth';
import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';

import {AF} from "../providers/af";

@Component({
  selector: 'app-register',
  moduleId: module.id.toString(),
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  public error: any;

  constructor(
    private afService: AF,
    private afAuth: AngularFireAuth,
      private firebaseAuth: AngularFireAuth,
      private router: Router,

  ) { }

  register(event, fullname, email, password) {
    event.preventDefault();
    this.afService.registerUser(email, password).then((user) => {
      this.afService.saveUserInfoFromForm(user.uid, fullname, email).then(() => {
        this.router.navigate(['/home']);
      })
        .catch((error) => {
          this.error = error;
        });
    })
      .catch((error) => {
        this.error = error;
        console.log(this.error);
      });
  }

}
