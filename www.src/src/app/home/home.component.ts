import { Component, OnInit, ViewChild } from '@angular/core';

import { Course } from '../course';
import { CourseService } from '../course.service';

import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { MatSidenav } from '@angular/material/sidenav';
import { Key } from 'selenium-webdriver';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  lists: Observable<any[]>;
  @ViewChild('sidenav') sidenav: MatSidenav;
  mangkey: any[] = [];
  reason = '';

  close(reason: string) {
    this.reason = reason;
    this.sidenav.close();
  }

  constructor(public db: AngularFireDatabase, private courseService: CourseService) {
    this.lists = db.list('lists').valueChanges();

  }

  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));

  ngOnInit() {

  }
  // add new course
  add(newCourse: string) {
    var tem = this.db.list('lists');
    const getKey = tem.push({});
    if (newCourse === '') {
      alert("namecourse is exited or emty");
    }
    else {
      getKey.set({
        id: getKey.key,
        name: newCourse,
      });
    }
    this.mangkey.push({ getKey });
    console.log(this.mangkey);
  }



  // delete course
  del(id: string) {
    this.db.list('lists').remove(id);
    delete this.mangkey[id];
  }
  // update course
  
}