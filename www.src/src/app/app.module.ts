import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule,MatSelectModule,MatInputModule } from '@angular/material';
//use form-field in material
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatCardModule} from '@angular/material/card';
import {MatMenuModule} from '@angular/material/menu';
import {CdkTableModule} from '@angular/cdk/table';
import {MatGridListModule} from '@angular/material/grid-list';


import { AppComponent } from './app.component';
import { CoursesComponent } from './courses/courses.component';
import { CoursedetailComponent } from './coursedetail/coursedetail.component';
import { routing } from './/app-routing.module';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { CourseService } from './course.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CategoryService } from './category.service';

import { CategoryDetailComponent } from './category-detail/category-detail.component';
import { AngularFireModule } from 'angularfire2';
import { firebaseConfig } from '../environments/environment';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { DialogAddWordComponent } from './dialog-add-word/dialog-add-word.component';

import { AuthProvider } from './providers/auth/auth';
import { AF } from './providers/af';

@NgModule({
  declarations: [
    AppComponent,
    CoursesComponent,
    CoursedetailComponent,
    NavComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    CategoryDetailComponent,
    DialogAddWordComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule, MatCheckboxModule,
    MatFormFieldModule,
    AngularFireModule.initializeApp(firebaseConfig, "myWordApp"),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatCardModule,
    MatMenuModule,
    CdkTableModule,
    MatGridListModule
  ],
  
  providers: [
    CourseService,
    CategoryService,
    AuthProvider,
    AF
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }




