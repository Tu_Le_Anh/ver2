import { Injectable } from '@angular/core';
import { Category } from './category';
import { CATEGORIES } from './mock-categories';

@Injectable()
export class CategoryService {


  getAll(): Promise<Category[]>  {
    return new Promise(resolve => 
      resolve(CATEGORIES)
    );
  }

  getById(name): Promise<Category> {
    return new Promise(resolve => 
      resolve(CATEGORIES.find(category => category.name === name))
    );
  }

  add(data) {
    return new Promise(resolve => {
      CATEGORIES.push(data);
      resolve(data);
    });
  }

  delete(id) {
    return new Promise(resolve => {
      let index = CATEGORIES.findIndex(category => category.id === id);
      CATEGORIES.splice(index, 1);
      resolve(true);
    });
  } 

  update(data) {
    return new Promise(resolve => {
     
      resolve(data);
    });
  }

  constructor() { }


} 
