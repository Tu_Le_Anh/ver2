import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { Router, ActivatedRoute } from '@angular/router';
import {MatFormFieldControl} from '@angular/material';
import { FormsModule }   from '@angular/forms';
import { HomeComponent } from '../home/home.component';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from "../models/user";
import {FormBuilder, FormGroup, Validators, AbstractControl,FormControl} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    public info:string;
    // public formgroup:FormGroup;
    // username:AbstractControl;
    // password:AbstractControl;

   user = {} as User;

   constructor(
    public afAuth: AngularFireAuth,
  //public formbuilder:FormBuilder,

  private route: ActivatedRoute,
  private router: Router,

  ) {
      // this.formgroup = formbuilder.group({
      //     username: new FormControl('', Validators.compose([
      //         Validators.required,
              
      //         Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      //       ]))
      //   ,
      //   password: new FormControl('', Validators.compose([
      //     Validators.required,
      //     Validators.minLength(6),
        
      //   ]))
      //   });
      
      // this.username = this.formgroup.controls['username'];
      // this.password = this.formgroup.controls['password'];
   }

  ngOnInit() {
  }
  async login(user: User) {
    try {
      const result = await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
      if (result) {
        this.router.navigate(['/home']);
      }  
    }
    catch (e) {
      this.info="Wrong username or passowrd";
      console.log(e);
    }
  }


}
