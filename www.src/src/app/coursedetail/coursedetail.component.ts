import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Course }         from '../course';
import { CourseService }  from '../course.service';
import { element } from 'protractor';
import { Category } from '../category';
import { CategoryService } from '../category.service';


@Component({
  selector: 'app-coursedetail',
  templateUrl: './coursedetail.component.html',
  styleUrls: ['./coursedetail.component.css']
})
export class CoursedetailComponent implements OnInit {
  @Input() course: Course;
  newCourse: Course = {
    id: 0,
    name:''
  }

  category: Category[];

  isNew = true;
  del = true;
  constructor(
    private route: ActivatedRoute,
    private courseService: CourseService,
    private location: Location,
    private categoryService: CategoryService
  ) {}

  
  ngOnInit() {
    this.getCourse();
    this.getCategory();
  }

  getCourse(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    if (id > 0){
      this.courseService.getById(id)
      .then(course => this.course = course);
      this.isNew = false;
    } else {
      this.course = this.newCourse;
      this.isNew = true;
    }

  }

  getCategory(): void {
    this.categoryService.getAll()
    .then(category => this.category = category);
  }

  goBack(): void {
    this.location.back();
  }

 save(): void {
    this.courseService.update(this.course)
      .then(() => this.goBack());
  }

  add(): void {
    this.course.id=10;
    this.courseService.add(this.course).then();
  }

  addCate(name: string) {

    name = name.trim();
    if (!name) { return; }
    var newCategory = new Category;
    // newCategory.id = this.id++;
    newCategory.name = name;
    this.categoryService.add(newCategory).then(); 
  }

}
