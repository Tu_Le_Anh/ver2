import { Category } from './category';

export const CATEGORIES:Category[] = [
    {
        id: 1,
        name: 'Category1',
    },
    {
        id: 2,
        name: 'Category2',
    },
    {
        id: 3,
        name: 'Category3',
    },
    {
        id: 4,
        name: 'Category4',
    },
    {
        id: 5,
        name: 'Category5',
    },
    {
        id: 6,
        name: 'Category6',
    },
    {
        id: 7,
        name: 'Category7',
    },
    {
        id: 8,
        name: 'Category8',
    },
    {
        id: 9,
        name: 'Category9',
    }
];