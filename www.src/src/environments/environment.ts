// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyDDdqero7N8EhwipGiYPheAkH6Duj25BpQ",
    authDomain: "mywordapp-2cbf2.firebaseapp.com",
    databaseURL: "https://mywordapp-2cbf2.firebaseio.com",
    projectId: "mywordapp-2cbf2",
    storageBucket: "mywordapp-2cbf2.appspot.com",
    messagingSenderId: "277308695900"
};